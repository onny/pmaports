# Maintainer: Venji10 <bennisteinir@gmail.com>
# Co-Maintainer: Joel Selvaraj <jo@jsfamily.in>

# Reference: <https://postmarketos.org/devicepkg>
pkgname=device-xiaomi-beryllium
pkgdesc="Xiaomi Poco F1"
pkgver=4
pkgrel=0
url="https://postmarketos.org"
license="MIT"
arch="aarch64"
options="!check !archcheck"
depends="
	postmarketos-base
	mkbootimg
	soc-qcom-sdm845
	soc-qcom-sdm845-ucm
	postmarketos-update-kernel
"
makedepends="devicepkg-dev"
subpackages="
	$pkgname-nonfree-firmware:nonfree_firmware
	$pkgname-kernel-tianma:kernel_tianma
	$pkgname-kernel-ebbg:kernel_ebbg
	$pkgname-phosh
"

source="
	deviceinfo
	rootston.ini
"

build() {
	devicepkg_build $startdir $pkgname
}

package() {
	devicepkg_package $startdir $pkgname
}

nonfree_firmware() {
	pkgdesc="GPU, venus, modem firmware"
	depends="firmware-xiaomi-beryllium"
	mkdir "$subpkgdir"
}

kernel_tianma() {
	pkgdesc="Tianma Panel. To know which panel your device use and the status of the port, Visit the Poco F1 wiki page: https://wiki.postmarketos.org/wiki/Xiaomi_Poco_F1_(xiaomi-beryllium)"
	depends="linux-postmarketos-qcom-sdm845"
	devicepkg_subpackage_kernel $startdir $pkgname $subpkgname
}

kernel_ebbg() {
	pkgdesc="EBBG Panel. To know which panel your device use and the status of the port, Visit the Poco F1 wiki page: https://wiki.postmarketos.org/wiki/Xiaomi_Poco_F1_(xiaomi-beryllium)"
	depends="linux-postmarketos-qcom-sdm845"
	devicepkg_subpackage_kernel $startdir $pkgname $subpkgname
}

phosh() {
	install_if="$pkgname=$pkgver-r$pkgrel phosh"
	install -Dm644 "$srcdir"/rootston.ini \
		"$subpkgdir"/etc/phosh/rootston.ini
}

sha512sums="
5602f92d1f405931b81c0e281348d78f2abf7361a76764897b15f7621bed9720fb543e52cecab7e28d0f1d6a1e2a5ba13fb8a7b1b9d25aa6c84b4c4be8b35d21  deviceinfo
e0bbe6210198ec37a0f18fb7dec5dead4ad41693ad5b3c20731e68d4f9d8fdff393dcbd110e87564030f1326e12da8af58c020e9bc14eb9ca2c54224b962df7e  rootston.ini
"
